import tkinter as tk
from tkinter import ttk
import os
import yaml
import warnings
import subprocess
from tkinter import filedialog
import torch
import cv2
import traceback
from tkinter import messagebox
import threading

from ultralytics import YOLO
warnings.filterwarnings('ignore')

# 设置工作目录为程序所在目录
os.chdir(os.getcwd())
print(os.getcwd())

def long_running_task():

    # 获取用户输入的参数
    model_file = model_var.get()  # 获取模型文件名
    model_file = os.path.join("./ultralytics/cfg/", model_file)  # 拼接模型文件路径

    data_file = data_var.get()  # 获取数据集配置文件名
    data_file = os.path.join("./ultralytics/data/", data_file)  # 拼接数据集配置文件路径

    optimizer = optimizer_var.get()  # 获取优化器类型

    # 获取图像大小，默认为640
    imgsz = int(imgsz_var.get()) if imgsz_var.get() else 640

    # 获取训练的总轮数，默认为50
    epochs = int(epochs_var.get()) if epochs_var.get() else 50

    # 获取批处理大小，默认为4
    batch_size = int(batch_var.get()) if batch_var.get() else 4

    # 自动混合精度
    amp = amp_var.get() == '是'

    # 获取多少轮后关闭马赛克数据增强，默认为10
    close_mosaic = int(close_mosaic_var.get()) if close_mosaic_var.get() else 10

    # 获取是否恢复训练（从检查点继续训练），'是'代表True，其他为False
    resume = resume_var.get() == '是'

    # 获取是否使用多尺寸训练，'是'代表True，其他为False
    scale = scale_var.get() == '是'

    # 获取是否缓存数据，'是'代表True，其他为False
    cache = cache_var.get() == '是'

    # 获取项目名称
    project = project_var.get()

    # 获取实验名称
    name = name_var.get()

    # 创建YOLO模型实例
    model = YOLO(model=model_file)

    # 加载预训练权重,改进或者做对比实验时候不建议打开，因为用预训练模型整体精度没有很明显的提升
    load_model_var_value = load_model_var.get()  # 获取输入的路径或文件名
    if load_model_var_value:  # 如果 load_model_var 不为空
        model.load(load_model_var_value)  # 加载模型
        print(f"预训练权重 {load_model_var_value} 已加载。")
    else:
        print("未提供预训练权重路径，跳过加载。")

    # 更新状态标签，显示训练开始
    status_label.config(text="训练开始...")

    # 禁用按钮
    train_button.config(state='disabled')

    try:
        model.train(data=data_file,
                    imgsz=imgsz,  # 输入图像的大小
                    epochs=epochs,  # 训练的总轮数
                    batch=batch_size,  # 批处理大小
                    workers=0,  # 数据加载的工作线程数
                    device=int(device_var.get()) if device_var.get() and device_var.get().isdigit() else '',
                    # 确保device_var.get()不为空且为数字
                    optimizer=optimizer,  # 使用的优化器类型
                    close_mosaic=close_mosaic,  # 多少轮后关闭马赛克数据增强
                    resume=resume,  # 是否恢复训练
                    project=project,  # 项目名称
                    name=name_var.get(),  # 实验名称
                    single_cls=False,  # 是否使用单类检测
                    cache=cache,  # 是否缓存数据
                    amp=amp,
                    patience=50,
                    multi_scale=scale,#多尺度训练
                    freeze=None
                    )
    except Exception as e:
        print(f"错误: {e}")
    # 恢复按钮
    train_button.config(state='normal')
    # 更新状态标签，显示训练开始
    status_label.config(text="训练停止...")

def start_training():

    # 创建一个新线程来执行耗时任务
    thread = threading.Thread(target=long_running_task)
    thread.start()

def open_Video_button():
    # 获取指定的初始打开路径
    initial_path = os.path.abspath(os.path.join('.', project_var.get()))
    # 检查路径是否存在
    if not os.path.exists(initial_path):
        print(f"存放训练结果的路径 {initial_path} 不存在，使用当前工作目录作为初始路径。")
        initial_path = os.getcwd()
    # 弹出文件选择对话框，只显示 .pt 文件
    file_path = filedialog.askopenfilename(
        filetypes=[("PT files", "*.pt")],
        initialdir=initial_path,
        title="请选择需要测试 权重 文件",  # 修改对话框标题
    )
    if file_path:
        # 获取文件的绝对路径
        model_path = os.path.abspath(file_path)
        print(model_path)
        model = YOLO(model_path)
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        print("使用设备: %s" % device)
        model.to(device)
        batch_size = 8
        frames_rgb = []
        frames = []
        input_str = Video_var.get()
        # 先尝试将输入转换为整数，如果成功则认为是设备 ID
        try:
            device_id = int(input_str)
            print(f"输入的是设备 ID: {device_id}")
            cap = cv2.VideoCapture(device_id)
        except ValueError:
            print(f"输入的是文件路径: {input_str}")
            cap = cv2.VideoCapture(input_str)

        if not cap.isOpened():
            print("错误：无法打开视频.")
        else:
            while True:
                ret, frame = cap.read()
                if not ret:
                    print("已完成视频处理.")
                    break
                width, height, _ = frame.shape
                new_shape = [32 * int(height / 64), 32 * int(width / 64)]
                frame = cv2.resize(frame, new_shape)
                frames.append(frame)

                if len(frames) == batch_size:
                    with torch.no_grad():
                        results = model.predict(frames)

                    for i, result in enumerate(results):
                        annotated_image = result.plot()
                        cv2.imshow('Video 按 q键 退出', annotated_image)

                        if cv2.waitKey(1) & 0xFF == ord('q'):
                            cap.release()
                            cv2.destroyAllWindows()
                    frames.clear()
                    frames_rgb.clear()
            cap.release()
            cv2.destroyAllWindows()

    else:
        print("未选择测试权重文件")

def open_Image_button():
    # 获取指定的初始打开路径
    initial_path = os.path.abspath(os.path.join('.', project_var.get()))
    # 检查路径是否存在
    if not os.path.exists(initial_path):
        print(f"存放训练结果的路径 {initial_path} 不存在，使用当前工作目录作为初始路径。")
        initial_path = os.getcwd()
    # 弹出文件选择对话框，只显示 .pt 文件
    file_path = filedialog.askopenfilename(
        filetypes=[("PT files", "*.pt")],
        initialdir=initial_path,
        title="请选择需要测试 权重 文件",  # 修改对话框标题
    )
    if file_path:
        # 获取文件的绝对路径
        model_path = os.path.abspath(file_path)
        print(model_path)
        model = YOLO(model_path)
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        print("使用设备: %s" % device)
        model.to(device)
        batch_size = 8
        frames_rgb = []
        frames = []
        input_path = Image_var.get()
        if os.path.isfile(input_path):
            # 输入是图片文件
            image = cv2.imread(input_path)
            if image is None:
                print("错误：无法读取图片.")
                return
            with torch.no_grad():
                results = model.predict(image)

            cv2.imshow('Image Detection', results[0].plot())
            cv2.waitKey(0)
            cv2.destroyAllWindows()
        elif os.path.isdir(input_path):
            # 输入是文件夹
            image_extensions = ['.jpg', '.jpeg', '.png', '.bmp','jpeg']
            image_files = [os.path.join(input_path, f) for f in os.listdir(input_path)
                           if any(f.lower().endswith(ext) for ext in image_extensions)]
            if not image_files:
                print("文件夹中没有有效的图片文件。")
                return
            for index, img_file in enumerate(image_files):
                image = cv2.imread(img_file)
                if image is None:
                    print(f"错误：无法读取图片 {img_file}.")
                    continue
                with torch.no_grad():
                    results = model.predict(image)
                cv2.imshow('Image Detection', results[0].plot())
                if index == len(image_files) - 1:
                    print("已推理完所有图片，按任意键结束。")
                    cv2.waitKey(0)
                else:
                    cv2.waitKey(1000)  # 等待 1 秒
            cv2.destroyAllWindows()
        else:
            print("输入既不是有效的图片文件也不是有效的文件夹路径.")
    else:
        print("未选择测试权重文件")

def open_data_file():
    folder_path = os.path.abspath('./ultralytics/data/')
    status_label.config(text=f"打开文件夹: {folder_path}")
    subprocess.run(['explorer', folder_path])

def on_combobox_select(event):
    model_file = model_var.get()  # 获取模型文件名
    selected_file = os.path.join("./ultralytics/cfg/", model_file)  # 拼接路径
    try:
        with open(selected_file, 'r', encoding='utf-8') as file:
            data = yaml.safe_load(file)
            nc_value = data.get('nc')
            if nc_value is not None:
                nc_var.set(nc_value)
    except FileNotFoundError:
        print(f"文件 {selected_file} 未找到。")
    except yaml.YAMLError as e:
        print(f"解析 YAML 文件时出错: {e}")

def open_model_file():
    folder_path = os.path.abspath('./ultralytics/cfg/')
    status_label.config(text=f"打开文件夹: {folder_path}")
    subprocess.run(['explorer', folder_path])

def update_nc_value(model_file):
    """读取并更新模型配置文件中的 nc 值"""
    try:
        with open(model_file, 'r') as file:
            config = yaml.safe_load(file)
        nc = config.get('nc', 0)
        nc_var.set(str(nc))
    except Exception as e:
        status_label.config(text=f"读取 YAML 文件失败: {e}")
        nc_var.set('0')

def update_nc_in_yaml():
    """更新 YAML 配置文件中的 nc 值"""
    model_file = model_var.get()  # 获取模型文件名
    model_file = os.path.join("./ultralytics/cfg/", model_file)  # 拼接路径
    nc_value = int(nc_var.get())

    if nc_value > 0:
        try:
            with open(model_file, 'r') as file:
                config = yaml.safe_load(file)

            # 更新 nc 值
            config['nc'] = nc_value

            with open(model_file, 'w') as file:
                yaml.dump(config, file)

            status_label.config(text="训练类数已更新!")
        except Exception as e:
            print(f"更新 YAML 文件失败: {e}")
            status_label.config(text="更新训练类数失败！")
    else:
        status_label.config(text="训练类数必须大于 0！")

def Load_weights():
    # 打开文件选择对话框，选择 .pt 文件
    file_path = filedialog.askopenfilename(
        title="加载预训练权重",
        filetypes=[("预训练模型文件", "*.pt")]
    )

    if file_path:
        # 将文件名显示在编辑框中
        load_model_var.set(file_path)

        # 更新状态标签
        status_label.config(text="训练类数已更新!")

def load_model_show_tooltip(event):
    """显示 tooltip 信息"""
    tooltip = tk.Toplevel(root)
    tooltip.wm_overrideredirect(True)  # 去掉窗口装饰
    tooltip.wm_geometry(f"+{event.x_root + 10}+{event.y_root + 10}")  # 设置 tooltip 显示的位置

    label = tk.Label(tooltip, text="可選非必須（epochs）直接输入预训练权重名称会自动下载 如:yolo11n.pt", background="yellow", relief="solid", padx=5, pady=5)
    label.pack()

    # 关闭 tooltip
    tooltip.after(3000, tooltip.destroy)
def load_model_hide_tooltip(event):
    pass

def check_and_create_folders():
    """检查文件夹是否存在，不存在则创建"""
    dirs = [
        './ultralytics/cfg/',
        "./ultralytics/data/",

        './datasets/detect/images/test/',
        './datasets/detect/images/train/',
        './datasets/detect/images/val/',
        './datasets/detect/labels/test/',
        './datasets/detect/labels/train/',
        './datasets/detect/labels/val/',

        './datasets/seg/images/test/',
        './datasets/seg/images/train/',
        './datasets/seg/images/val/',
        './datasets/seg/labels/test/',
        './datasets/seg/labels/train/',
        './datasets/seg/labels/val/',

        './datasets/obb/images/test/',
        './datasets/obb/images/train/',
        './datasets/obb/images/val/',
        './datasets/obb/labels/test/',
        './datasets/obb/labels/train/',
        './datasets/obb/labels/val/',

        './datasets/pose/images/test/',
        './datasets/pose/images/train/',
        './datasets/pose/images/val/',
        './datasets/pose/labels/test/',
        './datasets/pose/labels/train/',
        './datasets/pose/labels/val/',

        './datasets/cls/images/test/',
        './datasets/cls/images/train/',
        './datasets/cls/images/val/',
    ]
    print(f"检测数据集目录结构：")
    print(f"├──datasets #数据集跟目錄")
    print(f"  └──detect/seg/obb/pose/cls #數據集類型")
    print(f"    └──images/labels #圖片文件/標定文件")
    print(f"      └──train/val/test #模型训练/验证/测试")
    print(f"通常，数据集的划分比例如下：")
    print(f"训练集【train】：70%-80% 用于训练模型。")
    print(f"验证集【val】：10%-15% 用于调整超参数（如学习率、批量大小等）和防止过拟合。")
    print(f"测试集【test】：10%-15% 仅在模型训练完成后使用，用于评估模型的泛化能力。")
    for directory in dirs:
        abs_directory = os.path.abspath(directory)  # 获取绝对路径
        if not os.path.exists(abs_directory):
            os.makedirs(abs_directory)
            print(f"创建目录: {abs_directory}")

print(f"数据集目录结构检测完毕")
print(f"##################################################################")
check_and_create_folders()

try:
    # 创建主窗口
    root = tk.Tk()
    root.title("AI 训练配置")

    # 设置软件图标
    icon_path = os.path.abspath('./myicon.ico')  # 替换为您的图标路径
    try:
        # 尝试加载图标
        root.iconbitmap(icon_path)
    except Exception as e:
        # 如果加载失败，打印错误信息并继续执行
        print(f"加载图标失败: {e}")

    # 模型文件路径
    model_var = tk.StringVar(value=r'')

    # 创建标签和输入框
    tk.Label(root, text="模型配置 (model.yaml)", anchor='w').grid(row=0, column=0, padx=10, pady=5, sticky='w')
    tk.Label(root, text="数据配置 (data.yaml)", anchor='w').grid(row=1, column=0, padx=10, pady=5, sticky='w')
    model_combobox = ttk.Combobox(root, textvariable=model_var)
    model_combobox.grid(row=0, column=1, padx=10, pady=5, sticky='ew')
    model_combobox.bind("<<ComboboxSelected>>", on_combobox_select)


    # 获取目录下的yaml文件并更新下拉框
    def update_model_combobox():
        model_dir = r"./ultralytics/cfg/"
        yaml_files = [f for f in os.listdir(model_dir) if f.endswith('.yaml')]
        if yaml_files:
            model_combobox['values'] = yaml_files
            model_var.set(yaml_files[0])  # 默认选择找到的第一个文件
        else:
            model_combobox['values'] = []
            model_var.set('')  # 没有文件时，保持为空


    update_model_combobox()

    # 添加按钮打开文件
    open_model_button = tk.Button(root, text="打开文件", command=open_model_file)
    open_model_button.grid(row=0, column=2, padx=10, pady=5, sticky='ew')

    # 数据配置下拉框
    data_var = tk.StringVar(value='data.yaml')
    data_combobox = ttk.Combobox(root, textvariable=data_var)
    data_combobox.grid(row=1, column=1, padx=10, pady=5, sticky='ew')


    # 获取目录下的yaml文件并更新下拉框
    def update_data_combobox():
        data_dir = r"./ultralytics/data/"
        yaml_files = [f for f in os.listdir(data_dir) if f.endswith('.yaml')]
        if yaml_files:
            data_combobox['values'] = yaml_files
            data_var.set(yaml_files[0])  # 默认选择找到的第一个文件
        else:
            data_combobox['values'] = []
            data_var.set('')  # 没有文件时，保持为空


    update_data_combobox()

    # 添加按钮打开文件
    open_data_button = tk.Button(root, text="打开文件", command=open_data_file)
    open_data_button.grid(row=1, column=2, padx=10, pady=5, sticky='ew')

    # 预训练权重
    load_model_button = tk.Button(root, text="选择预训练权重", command=Load_weights)
    load_model_button.grid(row=2, column=2, padx=10, pady=5, sticky='ew')

    # 输入预训练权重
    tk.Label(root, text="预训练权重", anchor='w').grid(row=2, column=0, padx=10, pady=5, sticky='w')
    load_model_var = tk.StringVar(value="")
    load_model_entry = tk.Entry(root, textvariable=load_model_var)
    load_model_entry.grid(row=2, column=1, padx=10, pady=5, sticky='ew')
    # 绑定鼠标悬浮事件
    load_model_entry.bind("<Enter>", load_model_show_tooltip)  # 当鼠标进入时显示 tooltip
    load_model_entry.bind("<Leave>", load_model_hide_tooltip)  # 鼠标离开时可以隐藏 tooltip，虽然我们没有显示关闭动作

    # 输入图像大小
    tk.Label(root, text="图像大小", anchor='w').grid(row=3, column=0, padx=10, pady=5, sticky='w')
    imgsz_var = tk.StringVar(value="640")
    imgsz_entry = tk.Entry(root, textvariable=imgsz_var)
    imgsz_entry.grid(row=3, column=1, padx=10, pady=5, sticky='ew')

    # 输入训练轮数
    tk.Label(root, text="训练轮数", anchor='w').grid(row=4, column=0, padx=10, pady=5, sticky='w')
    epochs_var = tk.StringVar(value="300")
    epochs_entry = tk.Entry(root, textvariable=epochs_var)
    epochs_entry.grid(row=4, column=1, padx=10, pady=5, sticky='ew')

    # 输入批处理大小
    tk.Label(root, text="批次处理大小", anchor='w').grid(row=5, column=0, padx=10, pady=5, sticky='w')
    batch_var = tk.StringVar(value="3")
    batch_entry = tk.Entry(root, textvariable=batch_var)
    batch_entry.grid(row=5, column=1, padx=10, pady=5, sticky='ew')

    # 是否自动混合精度
    tk.Label(root, text="自动混合精度", anchor='w').grid(row=6, column=0, padx=10, pady=5, sticky='w')
    amp_var = tk.StringVar(value="是")
    amp_combobox = ttk.Combobox(root, textvariable=amp_var, values=['是', '否'])
    amp_combobox.grid(row=6, column=1, padx=10, pady=5, sticky='ew')

    # 选择设备 (CPU/GPU)
    tk.Label(root, text="设备ID(默认自动选择)", anchor='w').grid(row=7, column=0, padx=10, pady=5, sticky='w')
    device_var = tk.StringVar(value='')  # 留空表示自动选择
    device_entry = tk.Entry(root, textvariable=device_var)
    device_entry.grid(row=7, column=1, padx=10, pady=5, sticky='ew')

    # 选择优化器
    tk.Label(root, text="优化器", anchor='w').grid(row=8, column=0, padx=10, pady=5, sticky='w')
    optimizer_var = tk.StringVar(value="SGD")
    optimizer_combobox = ttk.Combobox(root, textvariable=optimizer_var, values=["", "SGD", "Adam", "AdamW"])
    optimizer_combobox.grid(row=8, column=1, padx=10, pady=5, sticky='ew')

    # 输入 close_mosaic 参数
    tk.Label(root, text="Mosaic数据增强", anchor='w').grid(row=9, column=0, padx=10, pady=5, sticky='w')
    close_mosaic_var = tk.StringVar(value="10")
    close_mosaic_entry = tk.Entry(root, textvariable=close_mosaic_var)
    close_mosaic_entry.grid(row=9, column=1, padx=10, pady=5, sticky='ew')

    # 选择是否从上次中断继续训练
    tk.Label(root, text="从上次中断继续训练", anchor='w').grid(row=10, column=0, padx=10, pady=5, sticky='w')
    resume_var = tk.StringVar(value='否')
    resume_combobox = ttk.Combobox(root, textvariable=resume_var, values=['是', '否'])
    resume_combobox.grid(row=10, column=1, padx=10, pady=5, sticky='ew')

    # 输入项目文件夹名称
    tk.Label(root, text="训练结果文件夹", anchor='w').grid(row=11, column=0, padx=10, pady=5, sticky='w')
    project_var = tk.StringVar(value='runs/train')
    project_entry = tk.Entry(root, textvariable=project_var)
    project_entry.grid(row=11, column=1, padx=10, pady=5, sticky='ew')

    # 打开训练结果文件夹按钮
    open_sample_button = tk.Button(root, text="打开训练结果文件夹",
                                   command=lambda: subprocess.run(
                                       ['explorer', os.path.abspath(os.path.join('.', project_var.get()))]))
    open_sample_button.grid(row=11, column=2, padx=5, pady=10)

    # 输入训练结果命名
    tk.Label(root, text="结果文件夹名", anchor='w').grid(row=12, column=0, padx=10, pady=5, sticky='w')
    name_var = tk.StringVar(value="exp")
    name_entry = tk.Entry(root, textvariable=name_var)
    name_entry.grid(row=12, column=1, padx=10, pady=5, sticky='ew')

    # 选择是否多尺寸训练
    tk.Label(root, text="多尺寸训练", anchor='w').grid(row=13, column=0, padx=10, pady=5, sticky='w')
    scale_var = tk.StringVar(value='否')
    scale_combobox = ttk.Combobox(root, textvariable=scale_var, values=['是', '否'])
    scale_combobox.grid(row=13, column=1, padx=10, pady=5, sticky='ew')

    # 选择是否缓存数据
    tk.Label(root, text="数据预处理并缓存", anchor='w').grid(row=14, column=0, padx=10, pady=5, sticky='w')
    cache_var = tk.StringVar(value='否')
    cache_combobox = ttk.Combobox(root, textvariable=cache_var, values=['是', '否'])
    cache_combobox.grid(row=14, column=1, padx=10, pady=5, sticky='ew')

    # 输入训练类数
    tk.Label(root, text="训练类数 (nc)", anchor='w').grid(row=15, column=0, padx=10, pady=5, sticky='w')
    nc_var = tk.StringVar(value="80")
    nc_entry = tk.Entry(root, textvariable=nc_var)
    nc_entry.grid(row=15, column=1, padx=10, pady=5, sticky='ew')

    # 更新 YAML 文件中的训练类数
    update_nc_button = tk.Button(root, text="更新训练类数", command=update_nc_in_yaml)
    update_nc_button.grid(row=15, column=2, padx=10, pady=5, sticky='ew')

    # 开始训练按钮
    train_button = tk.Button(root, text="开始训练", command=start_training)
    train_button.grid(row=16, column=0, padx=5, pady=10, columnspan=2, sticky='ew')

    # 打开样本文件夹按钮
    open_sample_button = tk.Button(root, text="打开样本文件夹",
                                   command=lambda: subprocess.run(['explorer', os.path.abspath('./datasets/')]))
    open_sample_button.grid(row=16, column=2, padx=5, pady=10, sticky='ew')

    # 状态标签
    status_label = tk.Label(root, text="等待用户输入...", fg="blue")
    status_label.grid(row=17, column=0, columnspan=3, sticky='ew')

    # --------------------------------------------------------------------------------------
    # 验证视频
    tk.Label(root, text="验证视频(视频文件/相机ID)", anchor='w').grid(row=18, column=0, padx=10, pady=5, sticky='w')
    Video_var = tk.StringVar(value="TestVideo.mp4")
    Video_entry = tk.Entry(root, textvariable=Video_var)
    Video_entry.grid(row=18, column=1, padx=10, pady=5, sticky='ew')

    # 验证视频按钮
    Video_pt_button = tk.Button(root, text="选择模型", command=open_Video_button)
    Video_pt_button.grid(row=18, column=2, padx=10, pady=5, sticky='ew')

    # 验证图片
    tk.Label(root, text="验证图片(图片文件/图片文件夹)", anchor='w').grid(row=19, column=0, padx=10, pady=5, sticky='w')
    Image_var = tk.StringVar(value="TestImage.png")
    Image_entry = tk.Entry(root, textvariable=Image_var)
    Image_entry.grid(row=19, column=1, padx=10, pady=5, sticky='ew')

    # 验证图片按钮
    Image_pt_button = tk.Button(root, text="选择模型", command=open_Image_button)
    Image_pt_button.grid(row=19, column=2, padx=10, pady=5, sticky='ew')

    # 让列在窗口调整大小时自适应
    root.grid_columnconfigure(1, weight=1)

    # 运行主循环
    root.mainloop()
except Exception as e:
    # 获取异常的详细信息
    error_info = traceback.format_exc()
    # 显示错误信息的消息框
    messagebox.showerror("错误", f"程序出现错误：\n{error_info}")
    # 等待用户按下任意键
    input("按任意键退出...")
